import React from 'react';
import { StyleSheet, View, Modal, TextInput, TouchableOpacity, Text, Picker } from 'react-native';

export default class ModalNovoRemedio extends React.Component {

    state = {
        nomeRemedio: '',
        quantidadeRemedio: '',
        modoTomarRemedio: '',
        frequenciaRemedio: '',
    };

    onPressAddRemedio = () => {
        this.props.onAdd(this.state.nomeRemedio, this.state.quantidadeRemedio, this.state.modoTomarRemedio, this.state.frequenciaRemedio);

        this.setState({
            nomeRemedio: '',
            quantidadeRemedio: '',
            modoTomarRemedio: '',
            frequenciaRemedio: '',
        });
    };

    render() {
        return (

            <Modal visible={this.props.modalVisible} animationType={'fade'} transparent={true} onRequestClose={this.props.onCancel}>

                <View style={styles.container}>
                    <View style={styles.containerBox}>
                        <View style={styles.headerModal}>
                            <Text style={styles.textHeaderModal}>Adicione seu Remédio</Text>
                        </View>
                        <TextInput style={styles.boxInput}
                            autoFocus
                            placeholder="Nome Ex.: Dipirona 500 MG"
                            value={this.state.nomeRemedio}
                            onChangeText={nomeRemedio => this.setState({ nomeRemedio })}
                        />
                        <TextInput style={styles.boxInput}
                            placeholder="Quantidade Ex.: 20"
                            value={this.state.quantidadeRemedio}
                            keyboardType='numeric'
                            onChangeText={quantidadeRemedio => this.setState({ quantidadeRemedio })}
                        />
                        <TextInput style={styles.boxInput}
                            placeholder="Modo de tomar Ex.: 3 Comprimidos"
                            value={this.state.modoTomarRemedio}
                            onChangeText={modoTomarRemedio => this.setState({ modoTomarRemedio })}
                        />
                        <TextInput style={styles.boxInput}
                            keyboardType='numeric'
                            placeholder="Frequênicia Ex.: 3"
                            value={this.state.frequenciaRemedio}
                            onChangeText={frequenciaRemedio => this.setState({ frequenciaRemedio })}
                        />
                        <View style={styles.butonsContainer}>
                            <TouchableOpacity onPress={this.props.onCancel} style={[styles.button, styles.butonCancel]}>
                                <Text style={styles.butonText}>Cancelar</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.onPressAddRemedio} style={[styles.button, styles.butonAdd]}>
                                <Text style={styles.butonText}>Adicionar</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </View>
            </Modal >
        )
    };
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    headerModal: {
        padding: 20,
    },
    textHeaderModal: {
        fontWeight: 'bold',
        fontFamily: 'Roboto',
        fontSize: 20,
    },
    containerBox: {
        borderRadius: 10,
        backgroundColor: '#FFFF',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 25.
    },
    inputsQtd: {
        flex: 1,
        flexDirection: 'row',
    },
    butonAdd: {
        backgroundColor: '#2D6E12',
    },
    butonCancel: {
        backgroundColor: '#D46A6A',
    },
    button: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        margin: 10,
    },
    butonText: {
        fontWeight: 'bold',
    },
    butonsContainer: {
        flexDirection: 'row',
        marginTop: 10,
        height: 60,
    },
    boxInput: {
        alignSelf: 'stretch',
        height: 40,
        //width: 120,
        margin: 20,
        borderRadius: 5,
    }
});  