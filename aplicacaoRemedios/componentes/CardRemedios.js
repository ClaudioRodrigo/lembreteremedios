import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';

export default class CardRemdedios extends React.Component {
    render() {
        return (
            <View style={styles.cardBody}>
                <View style={styles.headerCard}>
                    <Text style={styles.nomeRemedio}>{this.props.nome}</Text>
                </View>

                <View style={styles.conteudo}>
                    <Image
                        source={{ uri: 'https://cdn6.aptoide.com/imgs/6/c/4/6c4816bd064cf2631b051c74816fcf15_icon.png?w=256' }}
                        style={styles.remedioIcon}
                    />
                    <View style={styles.remedioDados}>
                        <Text style={styles.labelDesc}>Quantidade:
                        <Text style={styles.labelInfo}> {this.props.quantidade}</Text>
                        </Text>

                        <Text style={styles.labelDesc}>Tomar:
                        <Text style={styles.labelInfo}> {this.props.modoTomar}</Text>
                        </Text>

                        <Text style={styles.labelDesc}>Frequência:
                        <Text style={styles.labelInfo}> {this.props.frequencia} ao dia</Text>
                        </Text>
                    </View>
                </View>
                <View style={styles.conteudoFooter}>
                    <Image
                        source={{ uri: 'https://png.icons8.com/metro/1600/overtime.png' }}
                        style={styles.iconCalendar}
                    />
                    <Text style={[styles.dateText, styles.dateDetalheText]}>{this.props.date}</Text>
                    <View style={styles.tomarButton}>
                        <TouchableOpacity onPress={() => this.setState({ modalVisible: true })}>
                            <Text style={styles.textTomarButton}>Tomar</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    cardBody: {
        margin: 10,
        marginBottom: 2,
        justifyContent: 'center',
        backgroundColor: 'powderblue',
        borderRadius: 10,
        borderBottomWidth: 3,
        borderBottomColor: '#CCC',
        borderLeftWidth: 3,
        borderLeftColor: '#CCC',
        width: 320,
        height: 180,
    },
    conteudo: {
        flex: 1,
        flexDirection: 'row',
        padding: 10,
    },
    conteudoFooter: {
        flex: 1,
        flexDirection: 'row',
        paddingTop: 5,
        marginTop: 30,
        borderTopWidth: 2,
        borderTopColor: 'skyblue',
    },
    dateText: {
        fontWeight: 'bold',
        fontFamily: 'Roboto',
    },
    dateDetalheText: {
        paddingLeft: 10,
        paddingTop: 15,
        paddingRight: 5,
        borderRightWidth: 2,
        borderRadius: 1,
        borderRightColor: 'skyblue',
        fontFamily: 'Roboto',
        height: 40,
    },
    iconCalendar: {
        width: 20,
        height: 20,
        marginTop: 11,
        marginLeft: 20,
    },
    tomarButton: {
        backgroundColor: 'skyblue',
        padding: 10,
        marginLeft: 20,
        marginRight: 5,
        marginBottom: 7,
        borderRadius: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#CCC',
        borderLeftWidth: 1,
        borderLeftColor: '#CCC',
        width: 75,
        height: 40,
    },
    textTomarButton: {
        fontSize: 18,
        fontWeight: 'bold',
    },
    headerCard: {
        marginBottom: 5,
        height: 30,
        alignSelf: 'stretch',
        backgroundColor: 'skyblue',
        alignItems: 'center',
        borderRadius: 4,
    },
    remedioDadosDados: {
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    nomeRemedio: {
        fontSize: 20,
        paddingBottom: 2,
        alignItems: 'center',
        fontWeight: 'bold',
    },
    qtdRemedio: {
        paddingLeft: 20,
        fontSize: 18,
    },
    labelDesc: {
        paddingLeft: 20,
        fontSize: 18,
        fontFamily: 'Roboto',
        fontWeight: 'bold',
    },
    labelInfo: {
        marginLeft: 40,
        fontSize: 18,
        //fontFamily: 'Roboto',
        //fontWeight: 'bold',
        color: '#574F4F',
    },
    doseRemedio: {
        paddingLeft: 20,
        fontSize: 18,
    },
    remedioIcon: {
        paddingLeft: 20,
        width: 40,
        height: 40,
    },
});
