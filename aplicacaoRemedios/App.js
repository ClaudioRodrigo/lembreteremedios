/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Alert, ScrollView, Image, TouchableOpacity, AsyncStorage } from 'react-native';
import CardRemdedios from './componentes/CardRemedios';
import ModalNovoRemedio from './componentes/ModalNovoRemedio';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};

export default class App extends Component<Props> {

  state = {
    modalVisible: false,
    remedios: [],
  };

  async componentDidMount() {
    const remedios = JSON.parse(await AsyncStorage.getItem("@LembreteRemedioApp4::remedios4")) || [];
    this.setState({ remedios });
  };

  addRemedio = async (nome, quantidade, modoTomar, frequencia) => {

    var data = new Date();
    var day = data.getDate();
    var year = '-' + data.getFullYear();
    var month = (data.getMonth() <= 9) ? '-0' + data.getMonth() : '-' + data.getMonth();
    var hour = (data.getHours() <= 9) ? '0' + data.getHours() : data.getHours();
    var minutes = (data.getMinutes() <= 9) ? ':0' + data.getMinutes() : ':' + data.getMinutes();
    var seconds = (data.getSeconds() <= 9) ? ':0' + data.getSeconds() : ':' + data.getSeconds();

    const remedio = {
      id: Math.floor((Math.random() * 10)),
      nomeRemedio: nome,
      date: day + month + year + '\t\t' + hour + minutes + seconds,
      quantidadeRemedio: quantidade,
      modoTomarRemedio: modoTomar,
      frequenciaRemedio: frequencia,
    }
    this.setState({
      modalVisible: false,
      remedios: [
        ...this.state.remedios,
        remedio,
      ].reverse()
    })
    await AsyncStorage.setItem("@LembreteRemedioApp4::remedios4", JSON.stringify([...this.state.remedios, remedio]));
    Alert.alert(nome + " cadastrado(a) com sucesso")
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image
            source={{ uri: 'http://www.stickpng.com/assets/images/588a64f5d06f6719692a2d13.png' }}
            style={styles.menuIcon}
          />
          <Text style={styles.headerText}>Meus Remédios</Text>
        </View>

        <ScrollView>
          {this.state.remedios.map((item, key) => (
            <CardRemdedios key={key}
              nome={item.nomeRemedio} quantidade={item.quantidadeRemedio} modoTomar={item.modoTomarRemedio}
              frequencia={item.frequenciaRemedio} date={item.date}>
            </CardRemdedios>)
          )}
        </ScrollView>

        <View style={styles.viewFooter}>
          <TouchableOpacity onPress={() => this.setState({ modalVisible: true })}>
            <Text style={styles.footerButton}>Novo</Text>
          </TouchableOpacity>
        </View>

        <ModalNovoRemedio modalVisible={this.state.modalVisible}
          onCancel={() => this.setState({ modalVisible: false })}
          onAdd={this.addRemedio}
        />
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFCFC',
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    //marginTop: 22,
    marginBottom: 13,
    paddingBottom: 8,
    alignSelf: 'stretch',
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: 'powderblue',
  },
  headerText: {
    marginTop: 22,
    paddingTop: 7,
    marginTop: 5,
    marginRight: 95,
    fontSize: 22,
    fontWeight: 'bold',
  },
  menuIcon: {
    width: 40,
    height: 40,
    marginLeft: 20,
    marginTop: 7,
  },
  remedioIcon: {
    width: 40,
    height: 40,
    marginLeft: 33,
  },
  viewFooter: {
    backgroundColor: 'powderblue',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    flexDirection: 'row',
    marginTop: 5,
  },
  footerButton: {
    backgroundColor: 'powderblue',
    fontSize: 22,
    borderRadius: 10,
    fontWeight: 'bold',
    borderRadius: 5,
    padding: 5,
  },
  historicoIcon: {
    width: 30,
    height: 30,
    marginTop: 5,
    marginLeft: 33,
  },
  buttonNew: {
    marginTop: 4,
    marginBottom: 2,
    padding: 10,
    borderRadius: 190,
    backgroundColor: '#8787D0',
    borderColor: '#8787D0'
  },
  textButtonNew: {
    fontSize: 10,
    fontWeight: 'bold',
  }
});